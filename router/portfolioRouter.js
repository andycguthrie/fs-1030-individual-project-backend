import express from "express";
import db from '../db.js'

const portfolioRouter = express.Router();


portfolioRouter.get('/api/portfolio', (req,res) => {
    db.query("SELECT * FROM portfolio", function (error, results, fields) {
      if (res.status >= 400) {
        alert("Error")
      } else {
        return res.status(201).json(results);
          }
        }
      )
  })

  portfolioRouter.get("/api/portfolio/:id", (req, res) => {
    db.query(
      `SELECT * FROM portfolio WHERE id=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  portfolioRouter.delete("/api/portfolio/:id", (req, res) => {
    db.query(
      `DELETE FROM portfolio WHERE id=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  portfolioRouter.post("/api/portfolio", (req, res) => {
    db.query(
      "INSERT INTO portfolio (project, client, photo, description, tag1, tag2, tag3) VALUES (?, ?, ?, ?, ?, ?, ?)",
      [req.body.project, req.body.client, req.body.photo, req.body.description, req.body.tag1, req.body.tag2, req.body.tag3],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(201).send(results);
      }
    );
  });



      

export default portfolioRouter;