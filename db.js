import mysql from 'mysql';

const db = mysql.createConnection ({
    host: 'localhost',
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME

});

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});

export default db