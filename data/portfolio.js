let portfolio = [
 {id: 1,
     project: "Blog Re-Design",
     client: "The Campfire Collective",
     photo: "https://lh3.googleusercontent.com/7WjS8eCm0LDvDFVEFU-FhVtEDiK-KNBG5viW7z2uIyxvi2UyjQeJcW3wQ6Yta0GntH7zJGf6sQl2FznuWMz2IplrWkH_UjpI3uHTgeRuuF8-jRGFGyVI3YIRK_MJhm1HSAxelFEaGzQj8xE50sV1c1-CaCFDc2sRW8HNPYJqnaSZTDstG61fYDCOrsywzUSr-uH7-36twWzgDL_s2n-2BLymaf76b9x-9BfH367cve0uB6cBxZ2rzFFKIFXjfvxtlrNvh-YbHcrqaD1-CPh_8tQw7dpP6bnlJlXFeM3-LLZk9HkHF-vUUxQc9RuQLknwynwieXMSIWrSo4yuprAuCL3pvF3ZC3maVpP8i0PCMTVXgVsUGXCqxoDxgGNkZS_DvP5_pKFrflusorUIKCOxEkVigm6ULSUcgfdQk9U7UfTZ0oWyUhc1exlDQ1O-JvkAatfZw9UE_KRpdmOiVZIymfhBrQDODscxVeGi7n4e12xDS5LA3q5dpgYZhH66Vym6crunwfKgIe_nv2C6y0k8EG84S2a_jl0EFnJtqaBdznXVKYyyAbYGYke9lvMZIQLGa6fxB-lFdnS4fhmOr6VnMLiEcYoErdO7gKzZ24CVEgLoShqbMMaovTfyG_1JGAPJHLzjc1zBBBC4_dBJthPAxuNr4khbHAY-yaQEjzwrklMc4EIvgcec0HEuXHqCHB2m-H501uSop0TbQ9quiaI02bU=w1300-h867-no?authuser=0",
     desc: "Prepared animated and functional prototype for blog that outlined design, content organization and functionality using Sketch and Invision <br/> Interpreted strategic brand needs into a simplified filtering user interface <br/> Designed page layouts that met the User Experience needs",
     tag1: "UX",
     tag2: "UI",
     tag3: "Project Management"
 },
 
 {id: 2,
    project: "Student Life Re-Branding",
    client: "University of Toronto",
    photo: "https://lh3.googleusercontent.com/wKkIF82g38WCZdoyGmBmxBVHb8iUcbRHP5HA26oEi5Xh2Bmx-I3ITHXEK4cmFmMkT8wbcYOXOnhG73ukP4IjmAdYUqIOEckIWygrkyMmazcn99QFkTXyBbWzrQYhrgYpyiqgKelogbT_y4oUZvaymFeM2lddapDek_JeL9KITPrUzxc6sd5RP2IEEhW5aMV2kZP6z84en91pYQjj9im9z0J0YgHjhsv8jfHsUsXIBdlCD4SSNMuE4Gci1Uh0jF6DOvze77oLUWWPIYbzWlamyce-A2zJn9y330nQomoSs6f4WyBpXLwYe8P_WHBOi0wDLZJ5V_0JrWMaK0NoukUsWz2fV8RS__fiaDDoRw3-kl_LOkjxh8W1NRe0694EXTu54jDrLVUGO5g5HM5k4m-bgQ9hin5_pS8tAR0K4p3A6xQ8LGZ_WkKsXWIsxyA-R1PoSJSMB4c-BgfBxbFQQw1bYHTsx6NlDRKbuxUzAlGv4Q9schMoSyaGEB8upRc05Il-VPsuPU-AEu7eeyNrpjXVHvKXAK1NEP-N235YR0Y7mytGu2I1NyJ_MYGkjOs2tBigDu1IPW_A2-_nrkby8jyU1VQGosOP3Am-TvKvK-RMzH9qKh7dL8cmFxjNp6JRIOoVScmljmqn-WE8vu75cRehyiER4Uca3zuY7hItyngIHniTX2OM3q9A14OQkRn-nELgtWnt8nkqbt1cNC3gYmCOdzU=w1300-h867-no?authuser=0",
    b1: "Analyzed needs of 13 departments within U of T Student Life and prepared needs analysis presentation for Communications Director",
    b2: " Defined key problem with the brand and developed a proof of concept presentation",
    b3: "Presented proof of concept to 13 different lead teams gaining buy in from all",
    b4: "Improved productivity and brand consistency across Social, Multimedia, Web and Print marketing material",
    b5:"Liaised with design agency for design and collateral deliverables",
    tag1: "Brand",
    tag2: "Strategy",
    tag3: "Project Management"
},

{id: 3,
    project: "Website Design",
    client: "Kawartha Sexual Assault",
    photo: "https://lh3.googleusercontent.com/nV8mK3dGB9s7Jhr4EKdBl7njE3c-Rx99pLbVDldGXfZbx_VLgrT7sn8c3Re2xfEdE5OSMsBkgDaV0jNXfHF20llingcET9nvBg2iJfArPhWQwtC-Dvl6npYb7XZI9rNUmciEQqbwJnIK2mZ5lnXdabnGL058qWhk5ctEiq7cozd76liAP4tfCNF3n8anfXTvEOrbiC4hzCYJi0G_aYSzjtV35NPthNNbZu7SBGkHrUZKpsXaEfnO9gj6qwmJyWvVSsQD62YF_LkvceQAn3G98i7qgJ7jEXD6ztU0-GKwvuO-_r04GvTrhUOr0jrFO4SF8xotUvWkzsvf-VXZagJw162gRfQGf9TrXVKzB43TxtT1ePajI05fzKEfaPbs0ECI5SHC11oCg1cVni6PB5Iv_IQOKzHHJGoJQYJkr5ViYFrncP1YvSr61p7r6RE_EwcgE5jgHPvMTJlPKrxZEVvsgO7I-5CNfDqKjCBnuNQ4PxzX9bEpMcV-Ah8Q65EeAZtGhK3oqm3YgggkuQos4EpbTCkjE8iWmPhpvqC58Tt0qCc3HInGngwCq-1U5b_sg7J0wzi2s7ZlT1R1blzfm9136IJH8IM6njwFf_Fw7URR8QWjKc9yhi2_xWDLiYf6lzYTHfcLM3DZnfs_FmokvV-waaghfBwt_1b1hBnKchcXHbGhoqK9QgW4sbxj5N4JT5Diyhgaq8RSgor0U4ZQdK9w6YQ=w1300-h867-no?authuser=0",
    b1: "Strategized and presented UI/UX designs",
    b2: "Managed developers for web site.",
    b3: "Art directed and designed the website",
    tag1: "Teaching",
    tag2: "Web",
    tag3: "Project Management"
},

{id: 4,
    project: "Closer Campaign",
    client: "Peterborough Regional Health Centre",
    photo: "https://lh3.googleusercontent.com/oOwEu81Jmi4q2SSIecOKdHoipYyXt8tj4JqEX9WOfRohDm16PzGdK2-AZYidppci7yDW_TCbjebrcwWSszZ-8cPRztXOUICn0aCJm0yUr5pCcwnjawUirlbtsCaTtSQFJzsjG06HG9fJDTmOfmyDp2dFWbmkc6VnlQKsO_4NKhJfd7YDRf8n_zgG2skZynhCtFHX8O4DA290jy9gnaxJBig2V8-gg4sfABwKQ-YQz4e6M28UtURg89lgNcdKGDhRaKdKNWS1hfHJJa2U9uYLWKO5Ip-tWYszvAXpXbcQkHyowSjZMm8F2S3TRy-2yM-nu-CxuRTgdJ9T_3J2nBP4ykkFLZCIa13SmjXgssYZOGc7Luc6orhljPihRqg33EKTVgC80rtXNq7lYZkXzwK7Z3-FFWdT72400tkRtnklcmVSVJk5TTT54jfsq6f6yf2qzICxdvMF_yXFGSEpIF1-s9fE2PDYMd1IrMnuwxiKanHUggUZrwgrKYUdnELLIQ0Ru304m87lTrFyTAznEq5NAqtZo8-sIzeLPxjV2J8Fj7VMBj1rZ12jUyAVJM5xvb6BRy9yx1PPz1ByC1BPD1VBOofzQiDnKIPnVKgX2WpCzgcPOuGIebILgx8OOzUNI12zDxPdG0nwO9YdO3UuVZBzQWRUMCRcpty0GCnqyfHlkgjmZ2iINKH0OFdfD_gYA_54y-BBvlHTrQYY3mZqurXRYvs=w1920-h1280-no?authuser=0",
    b1: "Strategized and presented UI/UX designs",
    b2: "Managed developers for web site.",
    b3: "Art directed and designed the website",
    tag1: "Teaching",
    tag2: "Web",
    tag3: "Project Management"
},
 
 ]
 
 export default portfolio;